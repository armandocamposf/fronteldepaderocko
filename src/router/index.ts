import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '../views/HomeView.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView
    },
    {
      path: '/blog',
      name: 'posts',
      component: () => import('../views/BlogView.vue')
    },
    {
      path: '/post/:slug',
      name: 'post',
      component: () => import('../views/EntradaView.vue')
    },
    {
      path: '/contacto',
      name: 'contacto',
      component: () => import('../views/ContactoView.vue')
    },
    {
      path: '/libro-reclamaciones',
      name: 'reclamaciones',
      component: () => import('../views/ReclamacionesView.vue')
    },
    {
      path: '/registro',
      name: 'registro',
      component: () => import('../views/RegisterView.vue')
    },
    {
      path: '/confirmar-correo/:uuid',
      name: 'confirmar-correo',
      component: () => import('../views/RegisterConfirmView.vue')
    },
    {
      path: '/validar-correo/:uuid',
      name: 'validar-correo',
      component: () => import('../views/ConfirmarCorreoView.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('../views/LoginView.vue')
    },
    {
      path: '/reset-account',
      name: 'reset-account',
      component: () => import('../views/ResetAccountView.vue')
    },
    {
      path: '/pruebas',
      name: 'pruebas',
      component: () => import('../views/PruebasView.vue')
    },
    {
      path: '/registro/adicionales/:uuid',
      name: 'registros-adicionales',
      component: () => import('../views/MyPerfil1View.vue')
    },
    {
      path: '/perfil',
      name: 'perfil',
      component: () => import('../views/PerfilView.vue')
    },
    {
      path: '/nueva-clave/:uuid',
      name: 'nueva-clave',
      component: () => import('../views/NewPasswordView.vue')
    },
 
    {
      path: '/terminos-condiciones',
      name: 'terminos',
      component: () => import('../views/TerminosCondicionesView.vue')
    },
   
    {
      path: '/tratamiento-adicionales',
      name: 'tratamiento',
      component: () => import('../views/TratamientoAdicionalesView.vue')
    },

    {
      path: '/nosotros',
      name: 'nosotros',
      component: () => import('../views/NosotrosView.vue')
    }

  ]
})

export default router
